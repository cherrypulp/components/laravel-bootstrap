# Laravel Bootstrap

[![Build Status](https://travis-ci.org/cherrypulp/laravel-bootstrap.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-bootstrap)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/laravel-bootstrap/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/laravel-bootstrap/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/cherrypulp/laravel-bootstrap/badge.svg?branch=master)](https://coveralls.io/github/cherrypulp/laravel-bootstrap?branch=master)

[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-bootstrap.svg)](https://packagist.org/packages/cherrypulp/laravel-bootstrap)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-bootstrap/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-bootstrap)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-bootstrap.svg)](https://packagist.org/packages/cherrypulp/laravel-bootstrap)

Bootstrap components helpers for your Laravel/Stem project.

## Installation

Install via composer
```bash
composer require cherrypulp/laravel-bootstrap
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\LaravelBootstrap\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\LaravelBootstrap\Facades\LaravelBootstrap::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelBootstrap\ServiceProvider" --tag="config"
```

## Usage

This package have a list of common bootstrap components that you can use in different way.

Like a blade component : 

````blade
@component('bs:alert')
Message
@endcomponent
````

Like an aliased blade component (if config('laravel-bootstrap.register_components') is enabled)

````blade
@alert
Message
@endalert
````

Like a function helper (if config('laravel-bootstrap.helper) is true)

````php
bs()->alert('Message');
````

## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits

- [Daniel Sum](https://github.com/cherrypulp/laravel-bootstrap)
- [All contributors](https://github.com/cherrypulp/laravel-bootstrap/graphs/contributors)

This package is bootstrapped with the help of
[cherrypulp/laravel-package-generator](https://github.com/cherrypulp/laravel-package-generator).
