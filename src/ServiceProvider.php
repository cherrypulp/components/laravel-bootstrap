<?php

namespace Cherrypulp\LaravelBootstrap;

use Symfony\Component\Finder\SplFileInfo;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/laravel-bootstrap.php';

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'bs');

        $this->publishes([
            self::CONFIG_PATH => config_path('laravel-bootstrap.php'),
        ], 'config');

        if ($registerComponents = config("laravel-bootstrap.register_components")) {
            $this->registerComponents($registerComponents);
        }

        if (config("laravel-bootstrap.helper")) {
            include_once __DIR__ . '/helpers.php';
        }
    }


    public function registerComponents($registerComponents)
    {
        if (is_array($registerComponents)) {

            foreach ($registerComponents as $componentName) {
                \Blade::component("bs::" . $componentName, $componentName);
            }

        } elseif ($registerComponents) {

            $components = \File::files(__DIR__ . "/../resources/views");

            foreach ($components as $file) {
                /**
                 * @var $file SplFileInfo
                 */
                $componentName = str_replace(".blade", "", $file->getFilenameWithoutExtension());

                \Blade::component("bs::" . $componentName, $componentName);
            }
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'laravel-bootstrap'
        );

        $this->app->bind('laravel-bootstrap', function () {
            return new LaravelBootstrap();
        });
    }
}
