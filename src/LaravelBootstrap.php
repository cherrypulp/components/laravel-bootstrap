<?php

namespace Cherrypulp\LaravelBootstrap;

use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\HtmlString;

class LaravelBootstrap
{
    /**
     * @param $slot
     * @param string $class
     * @param string $type
     * @param bool $dismissible
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alert($slot, $class = '', $type = 'info', $dismissible = true){
        return view("bs::alert", get_defined_vars());
    }

    /**
     * @param $slot
     * @param string $class
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function badge($slot, $class = '', $type = 'info'){
        return view("bs::badge", get_defined_vars());
    }

    /**
     * @param $slot
     * @param string $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function breadcrumb($slot, $class = ''){
        return view("bs::breadcrumb", get_defined_vars());
    }

    /**
     * @param $slot
     * @param string $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buttonGroup($slot, $class = ''){
        return view("bs::button-group", get_defined_vars());
    }

    /**
     * @param $slot
     * @param string $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function card($slot, $class = ''){
        return view("bs::button-group", get_defined_vars());
    }

    /**
     * @param $items
     * @param null $slot
     * @param bool $controls
     * @param bool $indicators
     * @param null $id
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function carousel($items = null, $slot = null, $controls = true, $indicators = true, $id = null, $class = null){
        return view("bs::carousel", get_defined_vars());
    }

    /**
     * @param $slot
     * @param null $caption
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function figure($slot, $caption = null, $class = null){
        return view("bs::figure", get_defined_vars());
    }

    /**
     * @param $slot
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputGroup($slot, $class = null){
        return view("bs::input-group", get_defined_vars());
    }

    /**
     * @param $slot
     * @param null $heading
     * @param bool $fullwidth
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function jumbotron($slot, $heading = null, $fullwidth = true, $class = null){
        return view("bs::input-group", get_defined_vars());
    }

    /**
     * @param null $items
     * @param null $slot
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listGroup($items = null, $slot = null, $class = null){
        return view("bs::list-group", get_defined_vars());
    }

    /**
     * @param $slot
     * @param null $header
     * @param null $footer
     * @param null $class
     * @param null $id
     * @param null $animation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modal($slot, $header = null, $footer = null, $class = null, $id = null, $animation = null){
        return view("bs::modal", get_defined_vars());
    }

    /**
     * @param $slot
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pagination($slot, $class = null){
        return view("bs::pagination", get_defined_vars());
    }


    /**
     * @param $items
     * @param int $page
     * @param int $perPage
     * @param string $path
     * @param array $componentOptions
     * @return HtmlString
     */
    public function renderPagination($items, $page = 1, $perPage = 25, $path = '', array $componentOptions = [
        'class' => null,
        'arrows' => true
    ])
    {
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $paginator = (new LengthAwarePaginator($items, $items->count(), (int) $perPage, (int) $page))
            ->withPath($path);
        return $paginator->render('bs:::render.pagination', $componentOptions);
    }

    /**
     * @param $slot
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function progress($value, $slot = null, $min = null, $max = null, $class = null){
        return view("bs::progress", get_defined_vars());
    }

    /**
     * @param $slot
     * @param null $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function table($slot, $class = null){
        return view("bs::table", get_defined_vars());
    }
}