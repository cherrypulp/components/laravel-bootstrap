<?php

if (!function_exists("bs")) {
    /**
     * @return \Cherrypulp\LaravelBootstrap\LaravelBootstrap
     */
    function bs(){
        return app("laravel-bootstrap");
    }
}
