<?php

return [
    'helper' => true, // Enable the bs() helper or not
    'register_components' => true, // True or the array of registered components you want to register
];
