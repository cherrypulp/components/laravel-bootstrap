<div
        class="card {{ $class ?? '' }}"
>
    @isset($header)
        <div class="card-header">
            @slot('header')
                {!! $header !!}
            @endslot
        </div>
    @endisset

    {{ $slot }}

    @isset($footer)
        <div class="card-footer">
            @slot("footer")
                {!! $footer !!}
            @endslot
        </div>
    @endisset
</div>
